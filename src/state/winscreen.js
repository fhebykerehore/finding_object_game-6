/*global Phaser */
/*global game */
/*global scoreUser*/
/*global timerUser*/
/*global isWin*/
/*global gameLimit*/
/*global levelSetting*/
/*global totalLevel*/
/*global currentLevel*/

var winScreen = function(game) {
	this.game = game;
	console.log("%cStarting my awesome game", "color:white; background:red");
};

winScreen.prototype = {
	preload: function() {
		game.load.script('webfont', '//ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js');
		this.ingameData = levelSetting[currentLevel-1];

		this.game.load.atlasJSONHash('UI', 'asset/UI/ui.png', 'asset/UI/ui.json');
		this.game.load.atlasJSONHash('GAME_ASSET', 'asset/'+this.ingameData.folder_dest+'/JsonData.png', 'asset/' + this.ingameData.folder_dest + '/JsonData.json');
		this.game.load.image("black_bg", "asset/image/Solid_black.png");
	},
	create: function() {
		var background = this.game.add.sprite(0, 0, 'GAME_ASSET','background.jpg');
		background.width = this.game.width;
		background.height = this.game.height;

		this.black_bg = game.add.sprite(0, 0, 'black_bg');
		this.black_bg.width = game.width;
		this.black_bg.height = game.height;
		this.black_bg.alpha = 0.8;
		this.black_bg.inputEnabled = true;
		this.black_bg.events.onInputDown.add(this.nextLevel, this);

		var style = {
			font: "20px Montserrat",
			fill: "#FFFFFF",
			boundsAlignH: "right",
			boundsAlignV: "middle"
		};

		this.panelResult = this.game.add.sprite(0, 0, 'UI','pop up result.png');

		this.panelResult.height = 150;
		this.panelResult.width = 400;
		//

		this.panelResult.x = (this.game.width - this.panelResult.width) / 2;
		this.panelResult.y = this.game.height/2 - this.game.height/8;

		this.time_remaining_text = this.game.add.text(709, 364, "", style);
		this.time_remaining_text.text = this.parsingTime(timerUser);

		this.score_text = this.game.add.text(709, 383, "", style);
		this.score_text.text = scoreUser;

	},
	nextLevel: function() {
		console.log("current level is"+currentLevel);
		console.log("totalLevel is"+totalLevel);
		if(currentLevel==totalLevel)
		{
			currentLevel = 0;
			isOneCycle = true;
			this.game.state.start("MainMenu");
		}else{
			ajaxReq = $.ajax({
				type: 'POST',
				url: apiUrl+"/api/ho/game",
				dataType: "JSON",
				data:{
					level:currentLevel
				},
				success: function(resultData) {
					console.log(JSON.stringify(resultData));
					if(resultData.status == false)
					{
						alert("ERROR");
					}else{
						key_ = resultData.data.key;
						id_ = resultData.data.id;
						totalKoin_ = resultData.data.total_coin;
						game.state.start("Ingame");
					}
				}
			});

		}

	},
	makeDragAble: function(obj){
		obj.inputEnabled = true;
		obj.input.enableDrag();
		obj.events.onDragStop.add(this.onDragStop, this);
	},
	parsingTime: function(timers) {

		var seconds = (gameLimit - Math.floor(timers)) % 60;
		if (seconds < 10) {
			seconds = "0" + seconds;
		}
		var min = Math.floor((gameLimit - Math.floor(timers)) / 60);
		return  min + ":" + seconds;
	},
	onDragStop:function(sprite, pointer){
		console.log(sprite.x,sprite.y);
	}
}
